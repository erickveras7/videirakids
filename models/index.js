var mongoose = require('mongoose');
mongoose.connect("mongodb://localhost/videirakids");

var db = mongoose.connection;

db.on('error', function(err){
	console.log('Erro durante conexão: ', err);
});

db.on('open', function(){
	console.log('Conexão realizada com o banco');
});

exports.mongoose = mongoose;