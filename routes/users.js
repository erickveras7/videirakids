var express = require('express');
var router = express.Router();
var UserModel = require('../models/user');

router.list = function(req, res){
	console.log('router list');
	UserModel.list(req, res);
};

router.get = function (req, res){
	console.log('router get');
	UserModel.get(req, res);
}

router.create = function (req, res){
	console.log('router create');
	UserModel.create(req, res);
}

router.update = function (req, res){
	console.log('router update');
	UserModel.update(req, res);
}

router.delete = function (req, res){
	console.log('router delete');
	UserModel.delete(req, res);
}


module.exports = router;
