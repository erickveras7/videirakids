var express = require('express');
var router = express.Router();

/* GET home page. */
router.index = function(req, res) {
	res.render('index', { title: 'Videira Kids' });
};

router.partials = function (req, res) {
	var view_name = req.params.name;
	res.render('partials/' + view_name);
};

router.angular = function (req, res) {
	var directory = req.params.directory;
	var view_name = req.params.name;
	res.render(directory + '/' + view_name);
};

module.exports = router;
