var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/:name/:sexo', function(req, res) {
  	var name = req.params.name;
  	var sexo = req.params.sexo;

	var data = { 
	  	title: 'Videira Kids',

	  	name: name + ' do sexo: ' + sexo 
	};

	//res.render('login', data);
	res.json(data);
});

module.exports = router;
